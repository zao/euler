#pragma once
#include <iostream>
#include <gmp.h>
#include <string>
#include <type_traits>

#include <boost/operators.hpp>

#if defined(_DEBUG)
#pragma comment(lib, "mpir_d.lib")
#else
#pragma comment(lib, "mpir.lib")
#endif

struct Scalar : boost::operators<Scalar>
{
	mpz_t n;
	Scalar();

	template <typename T>
	Scalar(T const& arg, typename std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value, T*>::type = 0)
	{
		mpz_init_set_si(n, arg);
	}

	template <typename T>
	Scalar(T const& arg, typename std::enable_if<!(std::is_integral<T>::value && std::is_signed<T>::value), T*>::type = 0)
	{
		mpz_init_set_ui(n, (unsigned long)arg);
	}

	Scalar(std::string const& src, int base = 10);
	Scalar(char const* src, int base = 10);

	Scalar(Scalar const& rhs);
	~Scalar();

	Scalar& operator = (Scalar const& other);

	Scalar& operator ++ ();
	Scalar& operator -- ();

	template <typename T>
	typename std::enable_if<std::is_signed<T>::value, bool>::type
	operator < (T const& other) const
	{
		return mpz_cmp_si(n, other) < 0;
	}

	template <typename T>
	typename std::enable_if<!std::is_signed<T>::value, bool>::type
	operator < (T const& other) const
	{
		return mpz_cmp_ui(n, other) < 0;
	}

	bool operator < (Scalar const& other) const;

	Scalar& operator += (Scalar const& other);
	Scalar& operator -= (Scalar const& other);
	Scalar& operator *= (Scalar const& other);
	Scalar& operator /= (Scalar const& other);
	Scalar& operator %= (Scalar const& other);
	
	bool operator == (Scalar const& other) const;
};

std::string itoaz(Scalar const& s, int base = 10);
std::string::size_type countDigits(Scalar const& s);
std::ostream& operator << (std::ostream& os, Scalar const& s);