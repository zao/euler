#include "problem.h"
#include "conversion.h"
#include "primes.h"

#include <string>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graphviz.hpp>

template <>
void Problem<83>()
{
	typedef boost::property<boost::edge_weight_t, int> EdgeProperty;
	typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property, EdgeProperty> Graph;
	typedef Graph::vertex_descriptor Vertex;
	Graph G;
	
	int weights[80][80];
	std::vector<Vertex> vertices;
	std::ifstream is("../data/matrix.txt");
	for (int row = 0; row < 80; ++row)
	{
		std::string line;
		std::getline(is, line);
		std::istringstream iss(line);
		for (int col = 0; col < 80; ++col)
		{
			std::string valS;
			std::getline(iss, valS, ',');
			int val = atoiz(valS);
			weights[row][col] = val;
			assert(val > 0);
			vertices.push_back(add_vertex(G));
		}
	}

	for (int row = 0; row < 80; ++row)
	{
		for (int col = 0; col < 80; ++col)
		{
			Vertex self = vertices[row * 80 + col];
			if (col != 0)
			{
				Vertex left = vertices[row * 80 + (col - 1)];
				auto ed = add_edge(self, left, G);
				put(boost::edge_weight_t(), G, ed.first, weights[row][col - 1]);
			}
			if (col != 79)
			{
				Vertex right = vertices[row * 80 + (col + 1)];
				auto ed = add_edge(self, right, G);
				put(boost::edge_weight_t(), G, ed.first, weights[row][col+1]);
			}
			if (row != 0)
			{
				Vertex up = vertices[(row - 1) * 80 + col];
				auto ed = add_edge(self, up, G);
				put(boost::edge_weight_t(), G, ed.first, weights[row-1][col]);
			}
			if (row != 79)
			{
				Vertex down = vertices[(row + 1) * 80 + col];
				auto ed = add_edge(self, down, G);
				put(boost::edge_weight_t(), G, ed.first, weights[row+1][col]);
			}
		}
	}

	std::vector<Vertex> p(num_vertices(G));
	std::vector<int> d(num_vertices(G));
	dijkstra_shortest_paths(G, vertices.front(), boost::predecessor_map(&p[0]).distance_map(&d[0]));
	std::cerr << (weights[0][0] + d.back()) << std::endl;
}
