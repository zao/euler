#pragma once
#include "problem.h"
#include "primes.h"
#include "split.h"
#include "node.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include <boost/assign.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/property_map/vector_property_map.hpp>

template <typename T>
std::ostream& operator << (std::ostream& os, std::pair<T, T> const& pair)
{
	return os << "<" << pair.first << ", " << pair.second << ">";
}

#define PRIME_CEIL 1000000
#define PRIME_LIMIT 5

void analyse(std::set<Node> const& nodes, Node const& a, Node const& b)
{
}

void dump(std::set<Node> const& nodes, std::string filename, std::string const& title);
unsigned glue(unsigned a, unsigned b);

template <typename V, typename S>
bool testPrime(V const& primes, S const& value)
{
	for each (S prime in primes)
	{
		if (prime * prime > value)
			break;
		if (value % prime == 0)
			return false;
	}
	return true;
}

template <>
void Problem<60>()
{
	SievedPrimes sp(PRIME_CEIL);
	PrimeIterator first(sp), last;

#if 1
	using namespace boost::assign;
	std::vector<int> primes;
	while (first != last && *first <= 9999)
		primes += *first++;
	auto sz = primes.size();

	int minSum = std::numeric_limits<int>::max();
#define PRIME(IX) (primes[ix[IX]])
//#define NOT_PRIME(I1, I2) (!sp.testPrime(glue(primes[ix[I1]], primes[ix[I2]])))
#define NOT_PRIME(I1, I2) (!testPrime(primes, glue(primes[ix[I1]], primes[ix[I2]])))
#define BAAAH(IX) for (ix[IX] = ix[IX-1] + 1, ex[IX] = ex[IX-1] + 1; ix[IX] < ex[IX]; ++ix[IX])
	size_t ix[6] = {-1};
	size_t ex[6] = {sz - 6};
	BAAAH(1) {
		BAAAH(2) {
			if (NOT_PRIME(1,2) || NOT_PRIME(2,1)) continue;
			BAAAH(3) {
				if (NOT_PRIME(1,3) || NOT_PRIME(2,3) ||
				    NOT_PRIME(3,1) || NOT_PRIME(3,2)) continue;
				BAAAH(4) {
					if (NOT_PRIME(1,4) || NOT_PRIME(2,4) || NOT_PRIME(3,4) ||
					    NOT_PRIME(4,1) || NOT_PRIME(4,2) || NOT_PRIME(4,3)) continue;
					BAAAH(5) {
						if (NOT_PRIME(1,5) || NOT_PRIME(2,5) || NOT_PRIME(3,5) || NOT_PRIME(4,5) ||
						    NOT_PRIME(5,1) || NOT_PRIME(5,2) || NOT_PRIME(5,3) || NOT_PRIME(5,4)) continue;
						int sum = (PRIME(1)+PRIME(2)+PRIME(3)+PRIME(4)+PRIME(5));
						if (sum < minSum)
						{
							minSum = sum;
							std::cout << PRIME(1) << " " << PRIME(2) << " " << PRIME(3) << " " << PRIME(4) << " " << PRIME(5)
								<< " -> " << sum << std::endl;
						}
					}
				}
			}
		}
	}

#else
	std::set<Node> nodes;
	{
		using namespace boost::assign;
		std::vector<unsigned> tests = list_of(3)(7)(109)(673)(1097)(7109);
		bool good = true;
		for each (auto x in tests)
		{
			bool prime;
			if (sp.testPrime(x, prime) || !prime)
			{
				good = false;
			}
		}
		if (good)
		{
			std::cerr << "all good..." << std::endl;
		}
		else
		{
			std::cerr << "not good!" << std::endl;
		}
	}

	int i = 0;
	boost::uint32_t minSum = std::numeric_limits<boost::uint32_t>::max();
	while (first != last)
	{
		if (++i % 1000000 == 0)
			std::cerr << "at " << *first << std::endl;
		volatile auto current = *first;
		SplitIterator si(*first++, minSum), end;
		while (si != end)
		{
			auto pair = *si++;
			if (pair.first % 2 == 0 || pair.first % 5 == 0) 
				continue;
			bool primeA, primeB;
			if (!sp.testPrime(pair.first, primeA) && !sp.testPrime(pair.second, primeB) && primeA && primeB)
			{
				auto ba = boost::lexical_cast<int>(
					boost::lexical_cast<std::string>(pair.second) + boost::lexical_cast<std::string>(pair.first));

				bool primeBA;
				if (!sp.testPrime(ba, primeBA) && primeBA)
				{
					auto a = nodes.emplace(pair.first);
					auto b = nodes.emplace(pair.second);
					a.first->neighbours.insert(pair.second);
					b.first->neighbours.insert(pair.first);
					analyse(nodes, *a.first, *b.first);
				}
			}
		}
	}

	// prune away unpopular vertices
	{
		bool deleted = false;
		do
		{
			for each (auto node in nodes)
			{
				if (node.neighbours.size() < PRIME_LIMIT - 1)
				{
					for (auto J = node.begin(); J != node.end(); ++J)
					{
						auto K = nodes.find(*J);
						if (K != nodes.end())
							K->neighbours.erase(node.id);
					}
					node.neighbours.clear();
				}
			}

			deleted = false;
			for (auto I = nodes.begin(); I != nodes.end();)
			{
				if (I->neighbours.size() < PRIME_LIMIT - 1)
				{
					deleted = true;
					I = nodes.erase(I);
				}
				else
					++I;
			}
		} while (deleted);
	}

#if 0
	// compute strongly connected components
	{
		boost::uint32_t sz = (boost::uint32_t)nodes.size();

		typedef boost::adjacency_list<> Graph;
		Graph G(sz);
		boost::vector_property_map<boost::uint32_t> compMap(sz);

		size_t i = 0;
		std::map<Node::ID, size_t> idMap;
		for (auto I = nodes.begin(); I != nodes.end(); ++I, ++i)
		{
			idMap[I->id] = i;
		}

		for (auto I = nodes.begin(); I != nodes.end(); ++I)
		{
			for (auto N = I->begin(); N != I->end(); ++N)
			{
				add_edge(idMap[I->id], idMap[*N], G);
			}
		}

		boost::strong_components(G, compMap);
	}
#endif

	// brute force all possible cliques of size PRIME_LIMIT
	{
		int minSum = std::numeric_limits<int>::max();

		int cumSum = 0;
		auto I1 = nodes.begin();
		auto E1 = nodes.end(); std::advance(E1, 0 - PRIME_LIMIT);
		for (; I1 != E1; cumSum -= I1->id, ++I1)
		{
			cumSum += I1->id;
			if (cumSum > minSum) continue;

			auto I2 = I1; std::advance(I2, 1);
			auto E2 = nodes.end(); std::advance(E2, 1 - PRIME_LIMIT);
			for (; I2 != E2; cumSum -= I2->id, ++I2)
			{
				cumSum += I2->id;
				if (cumSum > minSum) continue;
				
				auto I3 = I2; std::advance(I3, 1);
				auto E3 = nodes.end(); std::advance(E3, 2 - PRIME_LIMIT);
				for (; I3 != E3; cumSum -= I3->id, ++I3)
				{
					cumSum += I3->id;
					if (cumSum > minSum) continue;

					auto I4 = I3; std::advance(I4, 1);
					auto E4 = nodes.end(); std::advance(E4, 3 - PRIME_LIMIT);
					for (; I4 != E4; cumSum -= I4->id, ++I4)
					{
						cumSum += I4->id;
						if (cumSum > minSum) continue;

#if PRIME_LIMIT == 5
						auto I5 = I4; std::advance(I5, 1);
						auto E5 = nodes.end(); std::advance(E5, 4 - PRIME_LIMIT);
						for (; I5 != E5; cumSum -= I5->id, ++I5)
						{
							cumSum += I5->id;
							if (cumSum > minSum) continue;
#endif
							using namespace boost::assign;
							std::vector<std::set<Node>::const_iterator> ns = list_of(I1)(I2)(I3)(I4)
#if PRIME_LIMIT == 5
								(I5)
#endif
								;
							//std::cerr << ns[0]->id << " " << ns[1]->id << " " << ns[2]->id << " " << ns[3]->id << std::endl;

							bool failed = false;
							for (size_t i = 0; i < PRIME_LIMIT; ++i)
							{
								for (size_t j = 0; j < PRIME_LIMIT; ++j)
								{
									if (i == j)
										continue;
									if (ns[i]->neighbours.count(ns[j]->id) == 0)
									{
										failed = true;
										break;
									}
								}
								if (failed)
									break;
							}
							if (!failed)
							{
								for each (auto I in ns)
									std::cerr << I->id << ' ';
								std::cerr << '(' << cumSum << ')' << std::endl;
								minSum = std::min(minSum, cumSum);
							}
#if PRIME_LIMIT == 5
						}
#endif
					}
				}
			}
		}
		std::cerr << "Minimal sum: " << minSum << std::endl;
	}

#if 0
	{
		std::cerr << "Dumping.... ";
		dump(nodes, "../" + boost::lexical_cast<std::string>(PRIME_CEIL) + ".dot", "59");
		std::cerr << "done." << std::endl;
	}
#endif
#endif
}