#pragma once
#include "problem.h"
#include "spam.h"

struct Context
{
	int m, d, a;
};

bool operator != (Context const& a, Context const& b)
{
	return a.m != b.m || a.d != b.d || a.a != b.a;
}

bool operator == (Context const& a, Context const& b)
{
	return !(a != b);
}

template <>
void Problem<64>()
{
	int odds = 0;
	for (int i = 1; i < 10000; ++i)
	{
		int S = i;
		int a0 = (int)sqrt((float)S);
		if (a0*a0 == S)
			continue;
		Context last = {};
		Context ctx = { 0, 1, a0 };

		std::vector<int> as;
		std::vector<Context> history;

		do
		{
			as.push_back(ctx.a);
			history.push_back(ctx);
			last = ctx;
			ctx.m = last.d * last.a - last.m;
			ctx.d = (S - ctx.m * ctx.m) / last.d;
			ctx.a = (a0 + ctx.m) / ctx.d;
		} while (std::find(history.begin(), history.end(), ctx) == history.end());
		
		if (as.size() % 2 == 0)
			++odds;
	}
	std::cout << odds << std::endl;
}