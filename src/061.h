#pragma once
#include "problem.h"
#include "spam.h"

namespace { enum Kind { Triangular, Square, Pentagonal, Hexagonal, Heptagonal, Octagonal }; enum { NumKinds = 6 }; }


std::map<int, std::set<int>> seqs[NumKinds];

enum { L = 1000, M = 100, U = 10000 };

typedef std::pair<int, int> SplitNumber;
SplitNumber split(int num)
{
	return std::make_pair(num / M, num % M);
}

bool isKind(Kind kind, SplitNumber num)
{
	auto I = seqs[kind].find(num.first);
	if (I != seqs[kind].end())
	{
		auto found = !!I->second.count(num.second);
		return found;
	}
	return false;
}

typedef std::uint8_t Kinds;

Kinds classify(SplitNumber const& num)
{
	Kinds ret = 0u;
	for (int kind = 0; kind < NumKinds; ++kind)
	{
		if (isKind((Kind)kind, num))
			ret |= 1 << kind;
	}
	return ret;
}

Kinds classify(short num)
{
	return classify(split(num));
}

SplitNumber operator + (SplitNumber const& a, SplitNumber const& b)
{
	auto aVal = a.first * M + a.second;
	auto bVal = b.first * M + b.second;
	return split(aVal + bVal);
}

 void recurse(int right, Kinds left, std::vector<SplitNumber> used)
 {
	if (used.size() == NumKinds)
	{
		if (used.begin()->first != used.rbegin()->second)
			return;
		SplitNumber acc;
		std::vector<int> ks;
		for each (auto n in used)
		{
			acc = acc + n;
			ks.push_back(classify(n));
		}
		std::cerr << used << " " << ks << " (" << acc << ")" << std::endl;
		exit(0);
	}

	for (int i = 10; i < M; ++i)
	{
		auto n = SplitNumber(right, i);
		if (std::find(used.begin(), used.end(), n) != used.end())
			 continue;
		Kinds k = classify(n) & left;
		if (!k)
			continue;
		for (int kind = 0; kind < NumKinds; ++kind)
		{
			Kinds bit = 1 << kind;
			if (k & bit)
			{
				auto myUsed = used;
				myUsed.push_back(n);
				recurse(n.second, left & ~bit, myUsed);
			}
		}
	 }
 }

template <>
void Problem<61>()
{
	std::function<int (int)> fs[] = {
		[](int n){ return n * (n + 1) / 2; },
		[](int n){ return n * n; },
		[](int n){ return n * (3 * n - 1) / 2; },
		[](int n){ return n * (2 * n - 1); },
		[](int n){ return n * (5 * n - 3) / 2; },
		[](int n){ return n * (3 * n - 2); }
	};

	for (int kind = 0; kind < NumKinds; ++kind)
	{
		auto f = fs[kind];
		for (int i = 1; ; ++i)
		{
			auto val = f(i);
			if (val < L)
				continue;
			if (val >= U)
				break;
			auto n = split(val);
			seqs[kind][n.first].insert(n.second);
		}
	}

	for (int i = L; i < U; ++i)
	{
		auto n = split(i);
		if (n.first < 10)
			continue;
		Kinds left = (1 << (NumKinds)) - 1;
		Kinds k = classify(i);
		if (!k)
			continue;
		left &= ~k;
		std::vector<SplitNumber> used;
		used.push_back(n);
		recurse(n.second, left, used);
	}
}