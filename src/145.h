#pragma once
#include "problem.h"
#include "conversion.h"

template <>
void Problem<145>()
{
	mpz_class count = 0;
	for (long n = 1; n < NUM4(1,000,000,000); ++n)
	{
		if (n % NUM2(100,000) == 0)
			std::cerr << (n / NUM2(100,000)) << std::endl;
		auto s = itoaz(n);
		if (s.back() == '0')
			continue;
		std::string rs(s.rbegin(), s.rend());
		auto rn = atoiz(rs);
		if (n >= rn)
			continue;

		std::string ssum = itoaz(n + rn);
		if (ssum.end() == std::find_if(ssum.begin(), ssum.end(), [](char ch)
		{
			return ((ch - '0') % 2) == 0;
		}))
			++count;
	}
	std::cerr << (count * 2) << std::endl;
}