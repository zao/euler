#pragma once
#include <string>
#include <type_traits>

#if defined(_DEBUG)
#pragma comment(lib, "libpngd.lib")
#pragma comment(lib, "zlibd.lib")
#else
#pragma comment(lib, "libpng.lib")
#pragma comment(lib, "zlib.lib")
#endif

template <typename T>
struct IsImage { static bool const value = false; };

template <typename Image>
typename std::enable_if<IsImage<Image>::value, size_t>::type width(Image const&);

template <typename Image>
typename std::enable_if<IsImage<Image>::value, size_t>::type height(Image const&);

template <typename Image>
typename std::enable_if<IsImage<Image>::value, double>::type pixel(Image const&, size_t row, size_t col);

#include <png++/png.hpp>

template <typename Image>
void saveImage(Image const& img, std::string const& filename)
{
	png::image<png::gray_pixel> out(width(img), height(img));
	for (int r = 0; r < out.get_height(); ++r)
	{
		for (int c = 0; c < out.get_width(); ++c)
		{
			out[r][c] = png::gray_pixel(255.0 * pixel(img, r, c));
		}
	}
	out.write(filename);
}