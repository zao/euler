#pragma once
#include "problem.h"
#include "conversion.h"
#include "spam.h"

std::map<int, int> chains;

int fac(int x)
{
	int acc = 1;
	for (int i = 2; i <= x; ++i)
		acc *= i;
	return acc;
}

namespace { enum { LOW = 1, HIGH = 1000000 }; }

template <>
void Problem<74>()
{
	int count = 0;
	
	for (int i = LOW; i < HIGH; ++i)
	{
		if (i % 10000 == 0)
			std::cerr << i << std::endl;
		
		std::set<int> used;
		std::vector<int> seq;
		int length = 0;

		int num = i;
		while (true)
		{
			auto I = chains.find(num);
			if (I != chains.end())
			{
				length += I->second;
				used.insert(num);
				seq.push_back(num);
			}
			if (used.find(num) != used.end())
			{
				if (length == 60)
					++count;

				break;
			}
			used.insert(num);
			seq.push_back(num);
			std::string s = itoaz(num);
			num = 0;
			for each (char ch in s)
			{
				num += fac(ch - '0');
			}
			++length;
		}
	}
	std::cout << count << std::endl;
}