#include "problem.h"

template <>
void Problem<104>()
{
	mpz_class Fs[3] = { 1, 1, 0 };
	mpz_class* F = Fs + 0;
	mpz_class* F_1 = Fs + 1;
	mpz_class* F_2 = Fs + 2;
	mpz_class mod("1000000000");
	mpz_class rem;
	int k = 1;
	while (true)
	{
		++k;
		if (k % 1000 == 0)
			std::cerr << k << std::endl;
		*F = *F_1 + *F_2;
		rem = *F % mod;
		std::string s = rem.get_str();
		if (s.size() >= 9)
		{
			std::set<char> digits(s.begin(), s.begin() + 9);
			if (digits.size() == 9 && *digits.begin() == '1' && *digits.rbegin() == '9')
			{
				s = F->get_str();
				digits.clear();
				digits.insert(s.begin(), s.begin() + 9);
				if (digits.size() == 9 && *digits.begin() == '1' && *digits.rbegin() == '9')
				{
					break;
				}
			}
		}
		std::swap(F, F_1);
		std::swap(F, F_2);
	}
	std::cout << "F_" << k << " is the number." << std::endl;
}