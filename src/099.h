#include "problem.h"
#include <boost/spirit/home/qi.hpp>

template <>
void Problem<99>()
{
	double best = 0;
	int bestRow = 0;
	int row = 1;

	std::ifstream is("../data/base_exp.txt");
	std::string line;
	while (std::getline(is, line))
	{
		using namespace boost::spirit::qi;
		auto first = line.begin(), last = line.end();
		double base, exponent;
		parse(first, last, int_ >> "," >> int_, base, exponent);

		double value = exponent * log(base);
		if (value > best)
		{
			best = value;
			bestRow = row;
			std::cerr << base << "^" << exponent << " == 10^" << value << std::endl;
		}

		++row;
	}
	std::cout << "Highest line is: " << bestRow << "." << std::endl;
}