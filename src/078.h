#pragma once
#include "problem.h"
#include "spam.h"
#include "scalar.h"
//#include "image.h"
#include "sequences.h"

std::map<Scalar, Scalar> partCache;

Scalar part(Scalar k)
{
	auto I = partCache.find(k);
	if (I != partCache.end())
		return I->second;

	GeneralizedPentagonalIterator gp;
	++gp;
	Scalar p;
	Scalar sign[] = { 1, -1 };
	
	for (Scalar i = 0; *gp <= k; ++i, ++gp)
	{
		bool negative = i/2%2 == 1;
		p += sign[negative] * part(k - *gp);
	}
	return partCache[k] = p;
}

template <>
void Problem<78>()
{
	partCache[0] = partCache[1] = 1;
	for (int i = 1; ; ++i)
	{
		Scalar p = part(i);

		if (p % 1000000 == 0)
		{
			std::cerr << i << " (" << p << ")" << std::endl;
		}
	}
}
