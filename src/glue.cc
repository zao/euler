#include <boost/spirit/home/qi.hpp>
#include <boost/spirit/home/karma.hpp>

unsigned glue(unsigned a, unsigned b)
{
	using namespace boost::spirit;
	std::string s;
	boost::spirit::karma::generate(std::back_inserter(s), karma::uint_ << karma::uint_, a, b);
	auto first = s.begin(), last = s.end();
	
	unsigned ret;
	boost::spirit::qi::parse(first, last, qi::uint_, ret);
	return ret;
}