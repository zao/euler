#include "problem.h"
#include "conversion.h"

template <>
void Problem<92>()
{
	int count = 0;
	for (int i = 1; i < 10000000; ++i)
	{
		if (i % 100000 == 0)
			std::cerr << i << std::endl;

		int n = i;
		while (n != 1)
		{
			if (n == 89)
			{
				++count;
				break;
			}
			std::string s = itoaz(n);
			int sum = 0;
			std::for_each(s.begin(), s.end(), [&sum](unsigned char ch)
			{
				ch -= '0';
				sum += ch * ch;
			});
			n = sum;
		}
	}
	std::cout << "There are " << count  << " numbers below ten million sticking at 89." << std::endl;
}