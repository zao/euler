#pragma once
#include "problem.h"
#include "spam.h"
#include "sequences.h"

namespace {
	//enum { N = 3, allowedDigits = 9 };
	enum { N = 5, allowedDigits = 16 };
}

typedef std::uint16_t Value;

std::vector<Value> nums;
std::vector<Value> highDigits;

Value val(Value idx)
{
	return nums[idx];
}

Value tripletSum(Value base)
{
	auto last = (base + 3) % (2 * N);
	return val(base) + val(base + 1) + val(last);
}

template <typename T, typename C>
void appendDigits(C& cont, T val)
{
	std::vector<T> backwards;
	while (val)
	{
		backwards.push_back(val % 10);
		val /= 10;
	}
	std::copy(backwards.rbegin(), backwards.rend(), std::back_inserter(cont));
}

std::set<std::vector<Value>> all;

void doOne()
{
	Value edgeSum = tripletSum(0);
	Value lowest = nums[0];
	Value lowBase = 0;
	for (int i = 2; i < 2*N; i += 2)
	{
		auto sum = tripletSum(i);
		if (sum != edgeSum)
			return;
		if (val(i) < lowest)
		{
			lowBase = i;
			lowest = val(i);
		}
	}

	std::vector<Value> digs;
	auto wi = makeWrappingIterator(nums.begin(), nums.begin() + lowBase, nums.end());
	for (int i = 0; i < N; ++i, --wi)
	{
		appendDigits(digs, *wi);
		appendDigits(digs, *++wi);
		appendDigits(digs, *++++wi);
	}

	if (digs.size() != allowedDigits)
		return;

	all.insert(digs);

//	std::cerr << digs << " | ";
//	std::cerr << lowBase << std::endl;
	
	if (digs > highDigits)
		highDigits = digs;
}

template <>
void Problem<68>()
{
	int i = 0;
	std::generate_n(std::back_inserter(nums), 2*N, [&i]{ return ++i; });
	while (std::next_permutation(nums.begin(), nums.end()))
	{
		doOne();
	}

	for each (auto digs in all)
		std::cerr << digs << std::endl;
	
	for each (auto d in highDigits)
		std::cout << d;
	std::cout << std::endl;
}