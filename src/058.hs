module Main where
import Primes
import qualified Data.Set as S
import Data.List
import Debug.Trace

numbers :: Integer -> [Integer]
numbers n =
    let incr = 2*n
        side = 2*n + 1
        base = side^2
    in map (\x -> base - x*incr) [3,2,1,0]

count :: [[Integer]] -> Integer
count cs =
    let ps = primes 1000000000
        arePrime = map (map (isPrime ps)) cs
    in f (0,0,0) arePrime
    where
        f foo@(n,p,np) (c:cs) | p * 9 < np = 2*(n-1)+1
                          | otherwise =
            let numPrimes = length $ filter id c
            in f (n + 1, p + numPrimes, np + 4 - numPrimes) cs
    {-
    in count' ps (1,0,0) cs
    where
        count' :: S.Set Integer -> (Integer, Integer, Integer) -> [[Integer]] -> Integer
        count' ps (n,prime,unprime) (c:cs) | n > 4 && prime * 9 < unprime = 2*n+1
                                           | otherwise =
            let numPrime = genericLength $ map (isPrime ps) c
            in count' ps (n+1, prime + numPrime, unprime + 4 - numPrime) cs
    -}
main = do
    let corners = map numbers [1..]
        result = count corners
        ps = primes 1000000000
--    print $ map (\x -> (x, isPrime ps x)) [0..15]
    print result
