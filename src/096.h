#pragma once
#include "problem.h"

struct Puzzle : std::vector<int>
{
	int& operator () (int row, int col) { return (*this)[row * 9 + col]; }
	int const& operator () (int row, int col) const { return (*this)[row * 9 + col]; }
	struct Cell
	{
		Cell(Puzzle& puzzle, int cellY, int cellX) : puzzle(puzzle), cellY(cellY), cellX(cellX) {}

		int& operator () (int ix)
		{
			int xOff = ix % 3;
			int yOff = ix / 3;
			int x = cellX * 3 + xOff;
			int y = cellY * 3 + yOff;
			return puzzle(y, x);
		}

		int const& operator () (int ix) const
		{
			int xOff = ix % 3;
			int yOff = ix / 3;
			int x = cellX * 3 + xOff;
			int y = cellY * 3 + yOff;
			return puzzle(y, x);
		}

	private:
		Puzzle& puzzle;
		int cellY, cellX;
	};

	Cell cell(int y, int x) { return Cell(*this, y / 3, x / 3); }
};

std::ostream& operator << (std::ostream& os, Puzzle const& puzzle)
{
	for (int row = 0; row < 9; ++row)
	{
		for (int col = 0; col < 9; ++col)
		{
			os << puzzle(row, col);
			if (col % 3 == 2 && col < 8)
				os << "|";
		}
		os << std::endl;
		if (row % 3 == 2 && row < 8)
			os << "---+---+---\n";
	}
	return os;
}

bool solve(Puzzle& puzzle, int index);

template <>
void Problem<96>()
{
	int sum = 0;
	std::ifstream is("../data/sudoku.txt");
	std::string line;
	int numSolved = 0;
	while (std::getline(is, line))
	{
		Puzzle puzzle;
		for (int row = 0; row < 9; ++row)
		{
			std::getline(is, line);
			for (auto I = line.begin(); I != line.end(); ++I)
				puzzle += (*I) - '0';
		}
		bool solved = solve(puzzle, 0);
		++numSolved;
		std::cerr << puzzle << std::endl;
		std::cerr << numSolved << " puzzles solved." << std::endl;
		sum += puzzle[0] * 100 + puzzle[1] * 10 + puzzle[2];
	}
	std::cout << "The sum is: " << sum << "." << std::endl;
}

bool solve(Puzzle& puzzle, int index)
{
	if (index == puzzle.size())
		return true;
	if (puzzle[index] != 0)
		return solve(puzzle, index + 1);

	int x = index % 9;
	int y = index / 9;
	std::set<int> candidates = list_of(1)(2)(3)(4)(5)(6)(7)(8)(9);
	for (int ix = 0; ix < 9; ++ix)
	{
		candidates.erase(puzzle(y, ix));
		candidates.erase(puzzle(ix, x));
		candidates.erase(puzzle.cell(y, x)(ix));
	}
	for (auto I = candidates.begin(); I != candidates.end(); ++I)
	{
		puzzle[index] = *I;
		if (solve(puzzle, index + 1))
		{
			return true;
		}
	}
	puzzle[index] = 0;
	return false;
}