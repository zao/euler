#include "primes.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

int main(int argc, char* argv[])
{
	if (argc != 2)
		return 1;

	unsigned n = 0u;
	std::istringstream iss(argv[1]);
	iss >> n;

	Primes primes(n);
	std::ofstream out((std::string(argv[1]) + ".primes").c_str());
	for (auto I = primes.primes.begin(); I != primes.primes.end(); ++I)
	{
		out << *I << std::endl;
	}
}
