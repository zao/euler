#pragma once

struct Stopwatch
{
	Stopwatch();
	~Stopwatch();

	void tic();
	double toc();

private:
	Stopwatch(Stopwatch const&);
	Stopwatch& operator = (Stopwatch const&);

private:
	struct StopwatchImpl* p;
};