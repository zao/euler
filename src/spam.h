#pragma once
#include <iostream>
#include <set>
#include <utility>
#include <vector>

template <typename T1, typename T2>
std::ostream& operator << (std::ostream& os, std::pair<T1, T2> const& p)
{
	return os << "<" << p.first << "," << p.second << ">";
}

template <typename T>
std::ostream& operator << (std::ostream& os, std::vector<T> const& arr)
{
	if (arr.size() == 0)
		return os << "{}";
	os << '{';
	size_t N = arr.size();
	for (size_t i = 0 ; i < N - 1; ++i)
	{
		os << arr[i] << ", ";
	}
	return os << arr[N-1] << '}';
}

template <typename T>
std::ostream& operator << (std::ostream& os, std::set<T> const& s)
{
	std::vector<T> v(s.begin(), s.end());
	return os << v;
}

template <typename T>
std::ostream& operator << (std::ostream& os, std::multiset<T> const& s)
{
	std::vector<T> v(s.begin(), s.end());
	return os << v;
}