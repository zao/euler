#pragma once
#include "problem.h"

#include <cstdint>
#include <iostream>
#include <limits>

#include <boost/math/common_factor.hpp>

enum Kind
{
	None, Descending, Ascending, Bouncy
};

bool isBouncy(std::uint64_t val);

template <>
void Problem<112>()
{
	int numBouncy = 0, total = 0;
	for (int i = 1; i < std::numeric_limits<int>::max(); ++i)
	{
		if (isBouncy(i))
			++numBouncy;
		++total;
		if (100 * numBouncy / total >= 99)
			break;
	}
	auto hundreds = 100 * numBouncy;
	//auto divisor = boost::math::gcd(hundreds, total);
	std::cerr << total << ": " << hundreds / total << std::endl;
}

bool isBouncy(std::uint64_t val)
{
	std::uint8_t kind = None;
	std::uint8_t buf[std::numeric_limits<std::uint64_t>::digits10 + 1];
	
	for (int i = 0; val && kind != Bouncy; ++i)
	{
		buf[i] = val % 10;
		val /= 10;
		if (i != 0)
		{
			if (buf[i-1] < buf[i])
				kind |= Descending;
			if (buf[i-1] > buf[i])
				kind |= Ascending;
		}
	}

	return kind == Bouncy;
}