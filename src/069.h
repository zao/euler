#pragma once
#include "problem.h"
#include "sequences.h"
#include "primes.h"
#include "spam.h"

template <typename C>
void factors(SievedPrimes const& sp, C& out, Scalar num)
{
	out.clear();
	Scalar root;
	mpz_sqrt(root.n, num.n);
	PrimeIterator pi(sp);
	Scalar p;
	while ((mpz_set_ui(p.n, *pi++), p) <= num)
	{
		if (mpz_divisible_p(num.n, p.n))
		{
			out.push_back(p);
			do
			{
				mpz_divexact(num.n, num.n, p.n);
			} while (mpz_divisible_p(num.n, p.n));
		}
	}
}

template <>
void Problem<69>()
{
	Scalar best = 2;
	mpq_t bestRatio;
	mpq_init(bestRatio);
	mpz_set_ui(mpq_numref(bestRatio), 2);
	SievedPrimes sp(1000000);
	for (Scalar i = 2; i <= 1000000; ++i)
	{
		PrimeIterator pi(sp);
		std::vector<Scalar> iFactors;
		factors(sp, iFactors, i);
#if 1
		mpq_t totient;
		mpq_init(totient);
		mpq_set_z(totient, i.n);
		for each (auto factor in iFactors)
		{
			Scalar numerator = factor - 1;
			mpq_t ratio;
			mpq_init(ratio);
			mpz_set(mpq_numref(ratio), numerator.n);
			mpz_set(mpq_denref(ratio), factor.n);
			mpq_canonicalize(ratio);
			mpq_mul(totient, totient, ratio);
		}
		mpq_canonicalize(totient);
		mpz_set(mpq_denref(totient), i.n);
		mpq_inv(totient, totient);
		mpq_canonicalize(totient);
		if (mpq_cmp(bestRatio, totient) < 0)
		{
			mpq_set(bestRatio, totient);
			best = i;
		}
		mpq_clear(totient);
#else
		Scalar tot;
		for each (auto factor in iFactors)
		{
			tot *= factor - 1;
		}
		if (mpz_cmp(mpq_numref(bestRatio), tot.n) < 0)
			mpz_set(mpq_numref(bestRatio), tot.n);
#endif
		if (i % 10000 == 9999)
			std::cerr << i << "..." << std::endl;
	}

	std::cout << best << " - " << mpq_get_d(bestRatio) << std::endl;
	mpq_clear(bestRatio);
}