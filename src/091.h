#include "problem.h"

struct Vector
{
	int x, y;
};

int dot(Vector const& a, Vector const& b)
{
	return a.x * b.x + a.y * b.y;
}

template <>
void Problem<91>()
{
	size_t const limit = 50;
	int count = 0;
	Vector origin = { 0, 0 };
	for (int x0 = 0; x0 <= limit; ++x0)
	{
		for (int y0 = 0; y0 <= limit; ++y0)
		{
			for (int x1 = 0; x1 <= limit; ++x1)
			{
				for (int y1 = 0; y1 <= limit; ++y1)
				{
					if (x0 * y1 == x1 * y0)
						continue;
					Vector _10 = { x1 - x0, y1 - y0 };
					Vector _1 = { x1, y1 };
					Vector _0 = { x0, y0 };
					if (!dot(_10, _1) || !dot(_10, _0) || !dot(_1, _0))
						++count;
				}
			}
		}
	}
	std::cout << count/2 << std::endl;
}