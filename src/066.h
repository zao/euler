#include "problem.h"
#include "primes.h"

struct Context
{
	mpz_class a, p, q, P, Q;
};

template <>
void Problem<66>()
{
	mpz_class bestx  = 0;
	int bestD = 0;
	SievedPrimes sp(1000000);
	for (int D = 2; D <= 1000; ++D)
	{
		int S = D;
		int a0 = (int)sqrt((float)S);
		if (a0*a0 == S)
			continue;
		Context c0 = { a0, a0, 1, 0, 1 };
		
		Context ctx = {};
		ctx.P = a0;
		ctx.Q = D - a0*a0;
		ctx.a = (a0 + ctx.P) / ctx.Q;
		ctx.p = a0*ctx.a + 1;
		ctx.q = ctx.a;

		std::vector<Context> history(1, c0);
		Context last = {};
		while (ctx.p*ctx.p - D * ctx.q * ctx.q != 1)
		{
			history.push_back(ctx);
			auto const& n1 = history[history.size()-1];
			auto const& n2 = history[history.size()-2];

			ctx.P = n1.a*n1.Q - n1.P;
			ctx.Q = (D - ctx.P*ctx.P) / n1.Q;
			ctx.a = (a0 + ctx.P) / ctx.Q;
			ctx.p = ctx.a*n1.p + n2.p;
			ctx.q = ctx.a*n1.q + n2.q;
		}

		if (ctx.p > bestx)
		{
			bestx = ctx.p;
			bestD = D;
		}
	}
	std::cout << "The maximum X = " << bestx << " occurs at D = " << bestD << "." << std::endl;
}