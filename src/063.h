#pragma once
#include "problem.h"
#include "conversion.h"
#include "scalar.h"

template <>
void Problem<63>()
{
	int num = 0;
	std::set<Scalar> used;

	for (Scalar i = 1; i < 10; ++i)
	{
		Scalar acc = i;
		for (Scalar power = 1; ; ++power, acc *= i)
		{
			if (power != countDigits(acc))
				break;
			if (used.count(acc))
				continue;
			used.insert(acc);
			++num;
			std::cerr << i << "^" << power << " = " << acc << std::endl;
		}
	}
	std::cout << num << std::endl;
}