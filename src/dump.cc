#include "node.h"
#include <fstream>
#include <set>
#include <string>

#define PRIME_LIMIT 5

void dump(std::set<Node> const& nodes, std::string filename, std::string const& title)
{
	std::ofstream os(filename.c_str());
	os << "graph " << title << " {" << std::endl;
	os << "overlap = scale;" << std::endl;
	for (auto I = nodes.begin(); I != nodes.end(); ++I)
	{
//		if (I->degree() <= PRIME_LIMIT)
//			continue;
		for (auto II = I->begin(); II != I->end(); ++II)
		{
//			if ((*II)->degree() <= PRIME_LIMIT)
//				continue;
//			if (I->id < (*II)->id)
				os << I->id << " -- " << *II << ";" << std::endl;
		}
	}
	os << "}" << std::endl;
}