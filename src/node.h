#pragma once

#include <set>
#include <boost/cstdint.hpp>

struct Node
{
	typedef boost::uint32_t ID;
	Node(ID id) : id(id) {}

	struct ByID
	{
		bool operator () (Node const* a, Node const* b) const
		{
			return *a < *b;
		}
	};

	bool operator < (Node const& other) const
	{
		return id < other.id;
	}

	int degree() const
	{
		return (int)neighbours.size();
	}

	typedef std::set<ID> HoodSet;

	HoodSet::const_iterator begin() const
	{
		return neighbours.begin();
	}

	HoodSet::const_iterator end() const
	{
		return neighbours.end();
	}

	ID id;
	mutable HoodSet neighbours;
};