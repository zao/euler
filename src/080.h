#include "problem.h"
#include "primes.h"

template <>
void Problem<80>()
{
	mpf_set_default_prec(10000);
	mpf_class n;
	mpz_class sum;
	for (int i = 1; i <= 100; ++i)
	{
		n = mpf_class(i, 100000);
		n = i;
		mpf_sqrt(n.get_mpf_t(), n.get_mpf_t());
		if (mpf_integer_p(n.get_mpf_t()))
			continue;
		mp_exp_t ex;
		std::string s = n.get_str(ex, 10, 103);
		s.resize(100);
		std::cerr << s << " " << s.size() << std::endl;
		std::for_each(s.begin(), s.end(), [&sum](char ch)
		{
			sum += ch - '0';
		});
	}
	std::cerr << "The sum is: " << sum << "." << std::endl;
}