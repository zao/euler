#pragma once
#include <algorithm>
#include <cstdlib>
#include <deque>
#include <set>
#include <vector>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/cstdint.hpp>
#pragma warning(push)
#pragma warning(disable: 4244 4800)
#include <mpirxx.h>
#pragma warning(pop)

struct Primes
{
	explicit Primes(size_t N)
		: sieve(N, true)
	{
		sieve[0] = sieve[1] = false;
		for (size_t i = 0; i < N; ++i)
		{
			if (sieve[i])
			{
				primes.insert(i);
				for (size_t j = 2 * i; j < N; j = ((j + i < j) ? N : (j + i)))
				{
					sieve[j] = false;
				}
			}
		}
	}

	std::vector<bool> sieve;
	std::set<size_t> primes;
};

struct SievedPrimes
{
	explicit SievedPrimes(size_t n);

	typedef bool Exhausted;
	Exhausted testPrime(unsigned val, bool& result) const;
	bool testPrime(unsigned val) const;
	
private:
	std::vector<boost::uint8_t> sieved;
};

struct PrimeIterator : boost::iterator_facade<PrimeIterator, unsigned const, boost::forward_traversal_tag>
{
	PrimeIterator();
	explicit PrimeIterator(SievedPrimes const& sp);
	
private:
	friend class boost::iterator_core_access;

	void increment();
	bool equal(PrimeIterator const& other) const;
	unsigned const& dereference() const;

	SievedPrimes const* sp;
	unsigned current;
};

template <typename C>
void primeFactors(SievedPrimes const& sp, C& out, mpz_class num)
{
	out.clear();
	mpz_class root;

	PrimeIterator pi(sp);
	typename C::value_type p;
	while (true)
	{
		p = *pi++;
		if (mpz_cmp_ui(num.get_mpz_t(), p) < 0)
			return;
		if (mpz_divisible_ui_p(num.get_mpz_t(), p))
		{
			out.push_back(p);
			do
			{
				mpz_divexact_ui(num.get_mpz_t(), num.get_mpz_t(), p);
			} while (mpz_divisible_ui_p(num.get_mpz_t(), p));
		}
	}
}

template <typename C>
bool coprime(C const& ap, C const& bp)
{	
	typedef boost::remove_reference<decltype(*ap.begin())>::type elem_type;
	std::set<elem_type> test(ap.begin(), ap.end());
	for (auto I = bp.begin(); I != bp.end(); ++I)
		if (test.count(*I))
			return false;
	return true;
}

inline bool coprime(int a, int b)
{
	static SievedPrimes sp(1000000);
	std::deque<int> ap, bp;
	primeFactors(sp, ap, a);
	primeFactors(sp, bp, b);
	return coprime(ap, bp);
}