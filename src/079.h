#pragma once
#include "problem.h"

#include <algorithm>
#include <array>
#include <cstdint>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/graphviz.hpp>

#include <boost/function_output_iterator.hpp>

struct Key
{
	typedef std::pair<char, char> Ordering;
	std::set<Ordering> orderings;
	typedef boost::adjacency_list<boost::setS, boost::vecS, boost::directedS> Graph;
	typedef std::pair<int, int> Edge;
	typedef Graph::vertex_descriptor Vertex;
	Graph G;

	std::set<Edge> edges;

	void augment(char a, char b, char c)
	{
		auto va = (a - '0');
		auto vb = (b - '0');
		auto vc = (c - '0');
		edges.insert(Edge(va, vb));
		edges.insert(Edge(vb, vc));
	}

	std::string resolve()
	{
		G = Graph(edges.begin(), edges.end(), 10);

		{
			std::ofstream os("../079.dot");
			write_graphviz(os, G);
		}

		std::deque<char> buf;
		auto iter = boost::make_function_output_iterator([&buf](Vertex const& vtx)
		{
			buf.push_front((char)(vtx + '0'));
		});
		topological_sort(G, iter); //redundant, graph is ocularly inspectable
		
		return std::string(buf.begin(), buf.end());
	}
};

template <>
void Problem<79>()
{
	Key key;
	std::ifstream is("../data/keylog.txt");
	std::string line;
	while (std::getline(is, line))
	{
		key.augment(line[0], line[1], line[2]);
	}
	std::cerr << key.resolve() << std::endl;
}
