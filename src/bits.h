#pragma once
#include <deque>

inline void bitsSet(std::deque<bool>& bits, int x, int N)
{
	bits.clear();
	for (int i = 0; i < N; ++i)
	{
		bits.push_front(x & 1);
		x >>= 1;
	}
}
