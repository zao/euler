#include "problem.h"
#include "spam.h"

void properDivisors(std::vector<int>& divs, int n)
{
	divs.clear();
	divs.push_back(1);
	for (int i = 2; i < sqrt((double)n)+1; ++i)
	{
		if (n % i == 0)
		{
			divs.push_back(i);
			divs.push_back(n / i);
		}
	}
}

template <>
void Problem<95>()
{
	std::vector<int> divs;

	size_t bestCount = 0;
	int bestMin = 0;

	for (int i = 1; i <= 1000000; ++i)
	{
		if (i % 10000 == 0)
			std::cerr << i << std::endl;

		std::deque<int> chain(1, i);
		while (1)
		{
			properDivisors(divs, chain.back());
			int sum = std::accumulate(divs.begin(), divs.end(), 0);

			if (sum > 1000000)
				break;

			auto I = std::find(chain.begin(), chain.end(), sum);
			if (I != chain.end() && I != chain.begin())
				break;

			if (sum == chain.front())
			{
				if (bestCount < chain.size())
				{
					bestCount = chain.size();
					bestMin = *std::min_element(chain.begin(), chain.end());
					std::cout << "Longest chain of " << bestCount << " elements has minimum value " << bestMin << std::endl;
				}
				break;
			}
			chain.push_back(sum);
		}
	}
	std::cout << "Longest chain of " << bestCount << " elements has minimum value " << bestMin << std::endl;
}