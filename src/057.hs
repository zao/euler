module Main where
import Data.Ratio
import Debug.Trace

digs x = length . show $ x

{-
data MyRatio = MyRatio Integer Integer deriving (Eq, Show)

numGTden (MyRatio num den) = digs num > digs den

a % b = MyRatio a b

instance Num MyRatio where
	(MyRatio a b) + (MyRatio a' b') = (a*b' + a'*b) % (b*b')
instance Fractional MyRatio where
	(MyRatio a b) / (MyRatio a' b') = (a*b') % (b*a')

f 0 = 1 % 2
f n = 1 % 1 / (2%1 + f (n-1))

main = do
	print $ length $ filter numGTden $ map (\x -> (1 % 1) + f x) [0 .. 999]
-}

f 0 = 1 % 2
f n = 1%1 / (2%1 + f (n-1))

main = do
	print $ length $ filter (\x -> digs (numerator x) > digs (denominator x)) $ map (\x -> 1%1 + f x) [0..999]
