#pragma once
#include "problem.h"
#include "bignum.h"
#include <algorithm>
#include <iostream>

template <>
void Problem<56>()
{
	size_t digSum = 0u;
	for (int a = 1; a < 100; ++a)
	{
		BigNum<> n(a);
		BigNum<> num(a);
		for (int b = 2; b < 100; ++b)
		{
			num *= n;
			std::cerr << n << " ^ " << b;
			std::cerr << " = ";
			std::cerr << num;
		   	std::cerr << std::endl;
			std::string s = num.str();
			size_t sum = 0u;
			for (auto I = s.begin(); I != s.end(); ++I)
				sum += *I - '0';
			digSum = std::max(digSum, sum);
		}
	}
	std::cout << digSum << std::endl;
}
