#pragma once
#include "problem.h"
#include "primes.h"

namespace { enum { limit = 50000000 }; }

template <>
void Problem<87>()
{
	SievedPrimes sp(1000000);
	std::set<int> nums;
	Stopwatch sw;
	for (PrimeIterator I(sp); ; ++I)
	{
		int i = *I;
		if (i*i >= limit)
			break;
		for (PrimeIterator J(sp); ; ++J)
		{
			int j = *J;
			if (i*i + j*j*j >= limit)
				break;
			for (PrimeIterator K(sp); ; ++K)
			{
				int k = *K;
				int sum = i*i + j*j*j + k*k*k*k;
				if (sum >= limit)
					break;
				nums.insert(sum);
			}
		}
	}
	std::cerr << nums.size() << " in " << sw.toc() << "s." << std::endl;
}