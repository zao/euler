#pragma once
#include "scalar.h"
#include <boost/iterator/iterator_facade.hpp>

struct GeneralizedPentagonalIterator : boost::iterator_facade<GeneralizedPentagonalIterator, Scalar const, boost::forward_traversal_tag>
{
	GeneralizedPentagonalIterator() : n(0), val(0), positive(0) {}

private:
	friend class boost::iterator_core_access;
	
	void increment()
	{
		if (!positive)
			++n;
		positive = !positive;
		Scalar sign = positive ? 1 : -1;
		val = (3 * n * n - sign * n) / 2;
	}

	bool equal(GeneralizedPentagonalIterator const& other)
	{
		return n == other.n;
	}

	Scalar const& dereference() const
	{
		return val;
	}

	Scalar n, val;
	bool positive;
};

template <typename Iter>
struct WrappingIterator : public boost::iterator_facade<WrappingIterator<Iter>, typename Iter::value_type, boost::bidirectional_traversal_tag>
{
	WrappingIterator(Iter first, Iter at, Iter last)
		: first(first), at(at), last(last)
	{}

private:
	friend class boost::iterator_core_access;

	void increment()
	{
		if (++at == last)
			at = first;
	}

	void decrement()
	{
		if (at == first)
			--(at = last);
		else
			--at;
	}

	bool equal(WrappingIterator const& other)
	{
		return first == other.first && at == other.at && last == other.last;
	}

	typename Iter::value_type& dereference() const
	{
		return *at;
	}

	Iter first, at, last;
};

template <typename Iter>
WrappingIterator<Iter> makeWrappingIterator(Iter first, Iter at, Iter last)
{
	return WrappingIterator<Iter>(first, at, last);
}