#include "problem.h"
#include "spam.h"

template <typename C1, typename C2>
bool sufficient(C1 const& c, C2 const& rhs)
{
	std::set<int> matches;
	std::set_intersection(c.begin(), c.end(), rhs.begin(), rhs.end(), std::inserter(matches, matches.end()));
	return matches.size() >= rhs.size();
}

template <typename C>
int make_num(C const& c)
{
	int num = 0;
	for (auto I = c.begin(); I != c.end(); ++I)
	{
		num *= 10;
		num += *I;
	}
	return num;
}

enum BasePolicy { Base, Parent, ParentSucc };

template <size_t N, BasePolicy Policy = Base>
struct For : For<N-1, Policy>
{
	typedef For<N-1, Policy> base_t;
	int i;
	void loop(int low, int high)
	{
		for (i = low; i < high; ++i)
		{
			auto base = low;
			if (Policy == Parent)
				base = i;
			else if (Policy == ParentSucc)
				base = i + 1;
			base_t::loop(base, high);
		}
	}
	
	int at(int x)
	{
		if (x == N)
			return i;
		return base_t::at(x);
	}
};

template <BasePolicy Policy>
struct For<1, Policy>
{
	virtual void f() = 0;
	int i;
	void loop(int low, int high)
	{
		for (i = low; i < high; ++i)
		{
			f();
		}
	}
	
	int at(int x)
	{
		return i;
	}
};

template <>
void Problem<90>()
{
	using namespace boost::assign;

	Stopwatch sw;
	struct AllCombos : For<6, ParentSucc>
	{
		std::set<std::set<int>> all;

		virtual void f()
		{
			std::set<int> current = list_of(at(1))(at(2))(at(3))(at(4))(at(5))(at(6));
			if (current.size() == 6)
				all.insert(current);
		}
	} combos;
	combos.loop(0, 10);

	std::cerr << combos.all.size() << std::endl;

	struct MatchCubes : For<2, Parent>
	{
		std::vector<std::set<int>> all;
		std::set<std::pair<int, int>> good;

		MatchCubes(std::set<std::set<int>>& all) : all(all.begin(), all.end())
		{
		}
		
		virtual void f()
		{
			auto a = at(1), b = at(2);
			auto const& lhs = all[a];
			auto const& rhs = all[b];

#define WHATABOUT(A, B) ((lhs.count(A) && rhs.count(B)) || (rhs.count(A) && lhs.count(B)))
			if (WHATABOUT(0, 1) &&
				WHATABOUT(0, 4) &&
				(WHATABOUT(0, 9) || WHATABOUT(0, 6)) &&
				(WHATABOUT(1, 6) || WHATABOUT(1, 9)) &&
				WHATABOUT(2, 5) &&
				(WHATABOUT(3, 6) || WHATABOUT(3, 9)) &&
				(WHATABOUT(4, 9) || WHATABOUT(4, 6)) &&
				(WHATABOUT(6, 4) || WHATABOUT(9, 4)) &&
				WHATABOUT(8, 1))
#undef WHATABOUT
			{
				good.insert(std::make_pair(a, b));
			}
		}
	} match(combos.all);
	match.loop(0, (int)match.all.size());

	for each (auto p in match.good)
	{
		//std::cerr << match.all[p.first] << ", " << match.all[p.second] << std::endl;
	}

	std::cout << match.good.size() << " in " << sw.toc() << "s." << std::endl;
}