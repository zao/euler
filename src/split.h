#pragma once
#include <boost/iterator/iterator_facade.hpp>
#include <boost/cstdint.hpp>
#include <set>

struct SplitIterator : boost::iterator_facade<SplitIterator, std::pair<boost::uint32_t, boost::uint32_t> const, boost::forward_traversal_tag>
{
	SplitIterator()
	{
		becomeEnd();
	}

	SplitIterator(boost::uint32_t num, boost::uint32_t limit)
		: num(num), limit(limit), mask(1)
	{
		increment();
	}

private:
	void becomeEnd()
	{
		num = limit = mask = 0;
	}

	friend class boost::iterator_core_access;

	void increment()
	{
		mask *= 10;
		while (mask < num)
		{
			auto div = num / mask;
			auto mod = num % mask;
			if (mod >= mask / 10)
			{
				current = std::make_pair(num / mask, num % mask);
				return;
			}
			mask *= 10;
		}
		becomeEnd();
	}

	bool equal(SplitIterator const& other) const
	{
		if (num != other.num) return false;
		return mask == other.mask;
	}

	std::pair<boost::uint32_t, boost::uint32_t> const& dereference() const
	{
		return current;
	}

	boost::uint32_t num, limit;
	boost::uint32_t mask;
	std::pair<boost::uint32_t, boost::uint32_t> current;
};