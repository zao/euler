#pragma once
#include <cstdint>
#include <string>

std::uint64_t atoiz(std::string const& s);
std::string itoaz(std::uint64_t val);