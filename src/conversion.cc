#include "conversion.h"
#include <boost/spirit/home/karma.hpp>
#include <boost/spirit/home/qi.hpp>
using namespace boost::spirit::karma;
using namespace boost::spirit::qi;

std::uint64_t atoiz(std::string const& s)
{
	auto first = s.begin(), last = s.end();
	std::uint64_t ret;
	parse(first, last, ret);
	return ret;
}

std::string itoaz(std::uint64_t val)
{
	std::string ret;
	ret.reserve(std::numeric_limits<std::uint64_t>::digits10);
	generate(std::back_inserter(ret), val);
	return ret;
}