#include "stopwatch.h"

#if defined(_WIN32)
#include <Windows.h>

struct StopwatchImpl
{
	LARGE_INTEGER t;
};

Stopwatch::Stopwatch()
	: p(new StopwatchImpl)
{
	tic();
}

Stopwatch::~Stopwatch()
{
	delete p;
}

void Stopwatch::tic()
{
	QueryPerformanceCounter(&p->t);
}

double Stopwatch::toc()
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);

	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);

	auto ret = (long double)(now.QuadPart - p->t.QuadPart);
	ret /= freq.QuadPart;
	return (double)ret;
}

#else
Stopwatch::Stopwatch() {}
Stopwatch::~Stopwatch() {}

void Stopwatch::start()
{}

double Stopwatch::stop()
{
	return 0.0;
}
#endif