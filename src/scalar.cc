#include "scalar.h"

#include <cstdint>
#include <iostream>
#include <iomanip>
#include <gmp.h>
#include <string>
#include <type_traits>

#include <boost/algorithm/string.hpp>

#if defined(_DEBUG)
#pragma comment(lib, "mpir_d.lib")
#else
#pragma comment(lib, "mpir.lib")
#endif

Scalar::Scalar()
{
	mpz_init(n);
}

Scalar::Scalar(std::string const& src, int base)
{
	mpz_init_set_str(n, src.c_str(), base);
}

Scalar::Scalar(char const* src, int base)
{
	mpz_init_set_str(n, src, base);
}

Scalar::Scalar(Scalar const& rhs)
{
	mpz_init_set(n, rhs.n);
}

Scalar::~Scalar()
{
	mpz_clear(n);
}

Scalar& Scalar::operator = (Scalar const& other)
{
	if (this == &other)
		return *this;
	mpz_set(n, other.n);
	return *this;
}

Scalar& Scalar::operator ++ ()
{
	mpz_add_ui(n, n, 1);
	return *this;
}
	
Scalar& Scalar::operator -- ()
{
	mpz_sub_ui(n, n, 1);
	return *this;
}

bool Scalar::operator < (Scalar const& other) const
{
	return mpz_cmp(n, other.n) < 0;
}

Scalar& Scalar::operator += (Scalar const& other)
{
	mpz_add(n, n, other.n);
	return *this;
}

Scalar& Scalar::operator -= (Scalar const& other)
{
	mpz_sub(n, n, other.n);
	return *this;
}

Scalar& Scalar::operator *= (Scalar const& other)
{
	mpz_mul(n, n, other.n);
	return *this;
}

Scalar& Scalar::operator /= (Scalar const& other)
{
	mpz_div(n, n, other.n);
	return *this;
}

Scalar& Scalar::operator %= (Scalar const& other)
{
	mpz_mod(n, n, other.n);
	return *this;
}

bool Scalar::operator == (Scalar const& other) const
{
	return mpz_cmp(n, other.n) == 0;
}

std::string itoaz(Scalar const& s, int base)
{
	std::string txt(mpz_sizeinbase(s.n, base) + 2, '\0');
	mpz_get_str(&txt[0], base, s.n);
	txt.resize(strlen(txt.c_str()));
	return txt;
}

std::string::size_type countDigits(Scalar const& s)
{
	return itoaz(s).size();
}

std::ostream& operator << (std::ostream& os, Scalar const& s)
{
	int base = 10;
	return os << itoaz(s, base);
}
