#include "problem.h"

enum { limit = NUM3(100,000,000) };
SievedPrimes sp(1000000);

void compute(int mBase, int inc, long& outCount)
{
	mpz_class count = 0;
	mpz_class k, m, n = 1, perimeter, s;
	std::deque<int> mFactors, nFactors;
	for (m = mBase; m*m - n*n < limit; m += inc)
	{
		primeFactors(sp, mFactors, m);
		if (mpz_divisible_ui_p(m.get_mpz_t(), 100))
			std::cerr << m << std::endl;

		for (n = 1; m > n; ++n)
		{
			primeFactors(sp, nFactors, n);
			if (!coprime(mFactors, nFactors) || mpz_even_p(n.get_mpz_t()) == mpz_even_p(m.get_mpz_t()))
				continue;
			{
				mpz_class sides[] = { (m*m - n*n), (2*m*n), (m*m + n*n) };
				perimeter = sides[0] + sides[1] + sides[2];

				std::sort(sides, sides + 3);
				auto const& a = sides[0];
				auto const& b = sides[1];
				auto const& c = sides[2];
				s = b - a;
				if (mpz_divisible_p(c.get_mpz_t(), s.get_mpz_t()))
				{
					k = limit / perimeter;
					count += k;
				}
			}
		}
		n = 1;
	}
	outCount = count.get_si();
}

template <>
void Problem<139>()
{
	mpz_class count = 0;
	long counts[4] = { 1, 10, 100, 1000 };

	Stopwatch sw;
	boost::thread t0(boost::bind(&compute, 2, 4, boost::ref(counts[0])));
	boost::thread t1(boost::bind(&compute, 3, 4, boost::ref(counts[1])));
	boost::thread t2(boost::bind(&compute, 4, 4, boost::ref(counts[2])));
	boost::thread t3(boost::bind(&compute, 5, 4, boost::ref(counts[3])));
	t0.join();
	t1.join();
	t2.join();
	t3.join();
	count = counts[0] + counts[1] + counts[2] + counts[3];
	
	std::cout << "In " << sw.toc() << "s, the number of triangles are: " << count << std::endl;
}