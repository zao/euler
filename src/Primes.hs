module Primes where
import System.IO
import System.FilePath
import qualified Data.Set as S
import qualified Data.ByteString as BS
import Data.Bits
import System.IO.Unsafe
import Debug.Trace

{-
 -primesFrom :: FilePath -> IO [Integer]
 -primesFrom filename = do
 -    primeFile <- openFile filename ReadMode
 -    contents <- hGetContents primeFile
 -    return . map read . lines $ contents
 -
 -primes n = unsafePerformIO $ do
 -    set <- primesFrom (show n ++ ".primes")
 -    return $ S.fromList set
 -
 -isPrime ps val | val <= S.findMax ps = S.member val ps
 -               | val > S.findMax ps ^ 2 = error "too large value"
 -               | otherwise = let ps' = S.toList ps in not $ any (\x -> val `mod` x == 0) $ takeWhile (\x -> val <= x ^ 2) ps'
 -}

primes n = unsafePerformIO $ BS.readFile (show n ++ ".bits")

isPrime :: BS.ByteString -> Integer -> Bool
isPrime bs val | val <= (fromIntegral $ BS.length bs) * 8 =
    let (div', m) = divMod val 8
        mod' = fromIntegral m
        octet = BS.index bs (fromIntegral div')
    in octet .&. (shiftL 1 mod') /= 0
