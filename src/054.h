#pragma once
#include "problem.h"

#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <boost/assign.hpp>
using namespace boost::assign;

namespace Poker
{
	enum Rank
	{
		Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
	};

	enum Suit
	{
		Hearts, Spades, Diamonds, Clubs
	};

	enum Score
	{
		HighCard, OnePair, TwoPairs, ThreeOfAKind, Straight,
		Flush, FullHouse, FourOfAKind,
		StraightFlush, RoyalFlush
	};

	Rank getRank(char ch)
	{
		static std::map<char, Rank> lookup = map_list_of
			('2', Two) ('3', Three) ('4', Four)  ('5', Five)
			('6', Six) ('7', Seven) ('8', Eight) ('9', Nine)
			('T', Ten) ('J', Jack)  ('Q', Queen) ('K', King)
			('A', Ace);
		return lookup[ch];
	}

	Suit getSuit(char ch)
	{
		static std::map<char, Suit> lookup = map_list_of
			('H', Hearts) ('S', Spades) ('D', Diamonds) ('C', Clubs);
		return lookup[ch];
	}
}

struct Hand
{
	std::map<int, std::set<Poker::Rank>> ranks;
	std::map<int, std::set<Poker::Suit>> suits;
	Poker::Rank cards[5];
};

std::pair<Hand, Hand> parseHands(std::string const& line)
{
	std::istringstream iss(line);
	Hand hands[2] = {};
	for (int player = 0; player < 2; ++player)
	{
		Hand& hand = hands[player];
		std::map<Poker::Rank, int> ranks;
		std::map<Poker::Suit, int> suits;
		for (int card = 0; card < 5; ++card)
		{
			std::string text;
			iss >> text;
			Poker::Rank rank = Poker::getRank(text[0]);
			Poker::Suit suit = Poker::getSuit(text[1]);
			ranks[rank]++;
			suits[suit]++;
			hand.cards[card] = rank;
		}
		for (auto I = ranks.begin(); I != ranks.end(); ++I)
		{
			hand.ranks[I->second].insert(I->first);
		}
		for (auto I = suits.begin(); I != suits.end(); ++I)
		{
			hand.suits[I->second].insert(I->first);
		}
		std::sort(hands[player].cards, hands[player].cards + 5);
	}
	return std::make_pair(hands[0], hands[1]);
}

struct HandValue
{
	Poker::Score score;
	Poker::Rank scoringCard;
	Poker::Rank cards[5];

	bool operator < (HandValue const& other) const;
	bool operator > (HandValue const& other) const;
};

HandValue handValue(Hand hand)
{
	HandValue ret = {};
	std::copy(hand.cards, hand.cards + 5, ret.cards);
	bool isStraight = (hand.cards[0] == hand.cards[1] - 1
	                && hand.cards[1] == hand.cards[2] - 1
	                && hand.cards[2] == hand.cards[3] - 1
	                && hand.cards[3] == hand.cards[4] - 1);

	bool isFlush = hand.suits[5].size();

	// Royal flush or straight flush
	if (isStraight && isFlush)
	{
		ret.scoringCard = hand.cards[4];
		if (ret.scoringCard == Poker::Ace)
			ret.score = Poker::RoyalFlush;
		else
			ret.score = Poker::StraightFlush;
	}
	// Four of a kind
	else if (hand.ranks[4].size())
	{
		ret.score = Poker::FourOfAKind;
		ret.scoringCard = hand.cards[2];
	}
	// Full house
	else if (hand.ranks[3].size() && hand.ranks[2].size())
	{
		ret.score = Poker::FullHouse;
		ret.scoringCard = *hand.ranks[3].begin();
	}
	// Flush
	else if (isFlush)
	{
		ret.score = Poker::Flush;
		ret.scoringCard = hand.cards[4];
	}
	// Straight
	else if (isStraight)
	{
		ret.score = Poker::Straight;
		ret.scoringCard = hand.cards[4];
	}
	// Three of a kind
	else if (hand.ranks[3].size())
	{
		ret.score = Poker::ThreeOfAKind;
		ret.scoringCard = *hand.ranks[3].rbegin();
	}
	// Two pairs
	else if (hand.ranks[2].size() == 2)
	{
		ret.score = Poker::TwoPairs;
		ret.scoringCard = *hand.ranks[2].rbegin(); 
	}
	// One pair
	else if (hand.ranks[2].size() == 1)
	{
		ret.score = Poker::OnePair;
		ret.scoringCard = *hand.ranks[2].rbegin();
	}
	// High card
	else
	{
		ret.score = Poker::HighCard;
		ret.scoringCard = hand.cards[4];
	}
	return ret;
}

template <>
void Problem<54>()
{
	int p1 = 0;
#if 1
	std::ifstream source("data/poker.txt");
#else
	std::istringstream source(
			"7S 7H 7C 7D QH 1S 2S 3S 4S 5S" "\n"
			"1S 2S 3S 4S 5S 7S 7H 7C 7D QH"
			);
#endif
	std::istream& is(source);

	std::string line;
	while (std::getline(is, line))
	{
		auto hands = parseHands(line);
		if (handValue(hands.first) > handValue(hands.second))
		{
			++p1;
		}
	}
	std::cout << p1 << " wins for player 1." << std::endl;
}



bool HandValue::operator < (HandValue const& other) const
{
	if (score < other.score) return true;
	if (score > other.score) return false;
	if (scoringCard < other.scoringCard) return true;
	if (scoringCard > other.scoringCard) return false;
	for (int i = 4; i >= 0; --i)
	{
		if (cards[i] < other.cards[i]) return true;
		if (cards[i] > other.cards[i]) return false;
	}
	return false;
}

bool HandValue::operator > (HandValue const& other) const
{
	return other < *this;
}
