#pragma once
#include "problem.h"
#include "bignum.h"
#include <iostream>

bool isPalindrome(std::string const& s)
{
	auto cch = s.size();
	for (decltype(cch) i = 0; i < cch / 2; ++i)
	{
		if (s[i] != s[cch - i - 1])
			return false;
	}
	return true;
}

template <>
void Problem<55>()
#if 0
{
	std::string a, b, slask;
	while (std::cin >> a >> slask >> b)
	{
		BigNum<> num1(a), num2(b);
		std::cerr << num1 << " + " << num2 << " = ";
		std::cout << num1 + num2 << std::endl;
	}
}
#else
{
	int numLychrel = 0;

	for (int i = 1; i < 10000; ++i)
	{
		BigNum<> num(i);
		bool isLychrel = true;
		for (int pass = 1; pass < 50; ++pass)
		{
			auto rev = reverse(num);
//			std::cerr << num.str() << " + " << rev.str();
			num += rev;
//			std::cerr << " = ";
			std::cerr << num.str();
			std::cerr << std::endl;
			if (isPalindrome(num.str()))
			{
				isLychrel = false;
				break;
			}
		}
		numLychrel += isLychrel;
	}
	std::cerr << "Lychrel count: " << numLychrel << std::endl;
}
#endif
