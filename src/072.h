#pragma once
#include "problem.h"
#include "primes.h"
#include "stopwatch.h"
#include "spam.h"

namespace { enum { N = 1000000 }; }

template <>
void Problem<72>()
{
	Stopwatch sw;
	SievedPrimes sp(1000000000);
	std::cerr << sw.toc() << std::endl;
	
	mpz_class ways;
	std::vector<std::uint32_t> factors;
	for (int i = 2; i <= N; ++i)
	{
		factors.clear();
		primeFactors(sp, factors, i);
		mpq_class pWays = i;
		for each (mpz_class factor in factors)
			pWays *= mpq_class(factor - 1, factor);
		ways += pWays;
	}

	std::cout << ways << " in " << sw.toc() << "s. " << std::endl;
}