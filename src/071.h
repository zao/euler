#pragma once
#include "problem.h"
#include "scalar.h"
#include <mpirxx.h>

namespace { enum { N = 1000000 }; }

bool between(mpq_class const& candidate, mpq_class const& left, mpq_class const& right)
{
	return candidate >= left && candidate <= right;
}

mpq_class _3_7(3, 7);
mpq_class best;

void bs(int num, int low, int high)
{
	int mid = (high + low) / 2;
	mpq_class left(num, low), half(num, mid), right(num, high);
	left.canonicalize();
	half.canonicalize();
	right.canonicalize();

	if (between(_3_7, left, half))
	{
		bs(num, low, mid);
	}
	else if (between(_3_7, half, right))
	{
		bs(num, mid, high);
	}
	else
	{
		if (right > best && right < _3_7)
		{
			best = right;
		}
	}
}

template <>
void Problem<71>()
{
	mpq_class frac;
	for (int num = 1; num <= N; ++num)
	{
		int low = num + 1, high = N;
		
		bs(num, low, high);
	}

	std::cerr << best << std::endl;
}