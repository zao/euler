#pragma once
#include "problem.h"

#include <cstdint>
#include <fstream>
#include <iostream>

struct Point
{
	int x, y;
};

struct Triangle
{
	Point p[3];
};

std::istream& operator >> (std::istream& is, Point& p)
{
	char comma;
	return is >> p.x >> comma >> p.y;
}

std::istream& operator >> (std::istream& is, Triangle& t)
{
	char comma;
	return is >> t.p[0] >> comma >> t.p[1] >> comma >> t.p[2];
}

Point operator - (Point const& a, Point const& b)
{
	Point ret = { a.x - b.x, a.y - b.y };
	return ret;
}

std::int64_t cross2D(Point const& u, Point const& v)
{
	return u.y * v.x - u.x * v.y;
}

bool containsOrigin(Triangle const& tri)
{
	Point p = { 0, 0 };
	Point const& a = tri.p[0], b = tri.p[1], c = tri.p[2];
	auto pab = cross2D(p - a, b - a);
	auto pbc = cross2D(p - b, c - b);
	if (pab * pbc <= 0) return false;

	auto pca = cross2D(p - c, a - c);
	if (pab * pca <= 0) return false;
	return true;
}

template <>
void Problem<102>()
{
	int count = 0;
	std::ifstream is("../data/triangles.txt");
	Triangle tri;
	while (is >> tri)
	{
		if (containsOrigin(tri))
			++count;
	}
	std::cout << "Answer: " << count << std::endl;
}