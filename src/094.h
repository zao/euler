#pragma once
#include "problem.h"
#include "primes.h"

template <>
void Problem<94>()
{
	mpz_class perSum, area4sq, tmp;
#if 1
	for (long  a = 2; a <= 333335000; ++a)
	{
		if (a % 100000 == 0)
			std::cerr << a << std::endl;
		long  const& b = a;
		for (int f = -1; f <= 1; f += 2)
		{
			long  c = a + f;
			long  per = a + b + c;
			if (per > 1000000000)
				break;
			area4sq = per;
			area4sq *= (-a + b + c);
			area4sq *= (a - b + c);
			area4sq *= (a + b - c);

			if (mpz_divisible_ui_p(area4sq.get_mpz_t(), 16))
			{
				mpz_divexact_ui(tmp.get_mpz_t(), area4sq.get_mpz_t(), 16);
				if (mpz_perfect_square_p(tmp.get_mpz_t()))
					perSum += per;
			}
		}
	}
#else
	int billion = 1000000000;
	int last_a = 2;
	for (mpz_class A = 1; ; ++A)
	{
		mpz_class lhs = 16 * A * A + 1;
		mpz_class t2, t3, t4;
		mpz_class mid, side;
		mpz_class rhs1, rhs2;
		for (mpz_class a = last_a; a < 34000000; ++a)
		{
			t2 = a * a, t3 = t2 * a, t4 = t3 * a;
			mid = 3 * t4 - 2 * t2;
			side = 4 * t3 - 4 * a;
			rhs1 = mid - side; //3 * t4 - 4 * t3 - 2 * t2 + 4 * a;
			rhs2 = mid + side; //3 * t4 + 4 * t3 - 2 * t2 - 4 * a;
			int cmp1 = mpz_cmp(lhs.get_mpz_t(), rhs1.get_mpz_t());
			int cmp2 = mpz_cmp(lhs.get_mpz_t(), rhs2.get_mpz_t());
			int sum1 = a.get_ui() * 3 - 1;
			int sum2 = sum1 + 2;
			if (cmp1 == 0 && sum1 < billion)
			{
				perSum += sum1;
				std::cerr << a << ", " << a << ", " << (a-1) << " = " << sum1 << "; A = " <<  A <<  std::endl;
			}
			if (cmp2 == 0 && sum2 < billion)
			{
				perSum += sum2;
				std::cerr << a << ", " << a << ", " << (a+1) << " = " << sum2 << "; A = " <<  A <<  std::endl;
			}

			if (cmp1 < 0 && cmp2 < 0)
				break;
			last_a = a.get_ui();
		}
	}
#endif
	std::cout << "Perimeter sum is: " << perSum << "." << std::endl;
}