#pragma once
#include "problem.h"
#include "primes.h"
#include "stopwatch.h"
#include "spam.h"
#include "conversion.h"

namespace { enum { N = 10000000 }; }

struct Totients
{
	Totients(PrimeIterator pi, unsigned cap)
	{
		tots.resize(cap);
		tots[0] = tots[1] = 1;
		
		{
			unsigned i = 2;
			std::generate_n(tots.begin() + 2, cap - 2, [&i]{ return i++; });
		}
		
		
		for (unsigned prime = *pi; prime < cap; prime = *++pi)
		{
			for (unsigned i = prime; i < cap; i += prime)
			{
				mpq_class part(prime - 1, prime);
				part.canonicalize();
				tots[i] *= part;
			}
		}
	}

	std::vector<mpq_class> tots;
};

template <>
void Problem<70>()
{
	SievedPrimes sp(1000000000);
	Stopwatch sw;

	Totients tots(PrimeIterator(sp), N);

	mpz_class best = 2;
	mpq_class bestRatio(2, tots.tots[2]);
	for (int n = 2; n < N; ++n)
	{
		auto phi_n = tots.tots[n];
		{
			std::string s1 = phi_n.get_str();
			std::string s2 = itoaz(n);
			if (s1.length() != s2.length())
				continue;
			std::sort(s1.begin(), s1.end());
			std::sort(s2.begin(), s2.end());
			if (s1 != s2)
				continue;
		}
		auto n_phi_n = mpq_class(n, phi_n);
		if (n_phi_n < bestRatio)
		{
			bestRatio = n_phi_n;
			best = n;
		}
	}

	std::cout << best << " in " << sw.toc() << "s. " << std::endl;
}