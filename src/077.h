#pragma once
#include "problem.h"
#include "spam.h"
#include "primes.h"

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <map>
#include <numeric>
#include <set>

typedef int Ways;

std::map<std::pair<int, int>, Ways> memo;
static Ways memoize(int items, int cap, Ways ways)
{
	return memo[std::make_pair(items, cap)] = ways;
}

std::vector<int> primes(1);

Ways fencePost(int items, int cap)
{		
	if (cap > items)
		cap = items;

	auto I = memo.find(std::make_pair(items, cap));
	if (I != memo.end())
		return I->second;

	if (items == 0)
		return 1;
#if 0
	if (items <= 0)
		return memoize(items, cap, 1);
	if (items == 1 && cap == 1)
		return memoize(items, cap, 1);
#endif

	Ways ways = 0;
	for (int i = 1; primes[i] <= cap; ++i)
	{
		ways += fencePost(items - primes[i], std::min(primes[i], cap));
	}
	
	return memoize(items, cap, ways);
}

namespace { enum { PROBLEM_LIMIT = 100 }; }

template <>
void Problem<77>()
{
	SievedPrimes sp(1000000);
	PrimeIterator first(sp), last;
	std::copy(first, last, std::back_inserter(primes));
	
	Stopwatch sw;
	for (int to = 2; to <= PROBLEM_LIMIT; ++to)
	{
		Ways ways = 0;
		for (int i = 1; primes[i] < to; ++i)
		{
			ways += fencePost(to - primes[i] , primes[i]);
		}
		if (ways > 5000)
		{
			auto dt = sw.toc();
			std::cout << to << " (" << ways << " ways) in " << dt << "s." << std::endl;
			return;
		}
	}
}