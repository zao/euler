module Main where
import Data.List
import Data.Ratio

e = [2,1] ++ intercalate [1,1] [[x] | x <- [2,4..]]

cont [x] = x%1
cont (x:xs) = x%1 + 1%1 / cont xs

main = do
    let x = cont $ take 100 e
        digits n = map (\x -> read [x]) $ show n
    print (sum . digits $ numerator x)
