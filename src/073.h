#pragma once
#include "problem.h"
#include "primes.h"

template <>
void Problem<73>()
{
	mpq_class _1_3(1, 3), _1_2(1, 2);
	int count = 0;

	mpq_class n;
	for (int den = 2; den <= 12000; ++den)
	{
		for (int num = den/3; num < den * 2; ++num)
		{
			n.get_num() = num;
			n.get_den() = den;
			n.canonicalize();
			if (n.get_num() == num && _1_3 < n && n < _1_2)
				++count;
		}
	}
	std::cerr << count << std::endl;
}