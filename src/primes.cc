#include "primes.h"
#include <fstream>
#include <boost/lexical_cast.hpp>

SievedPrimes::SievedPrimes(size_t n)
{
	std::string file = boost::lexical_cast<std::string>(n) + ".bits";
	std::ifstream is(file.c_str(), std::ios::binary);
	
	if (!is) is.open(("../" + file).c_str(), std::ios::binary);
	if (!is) throw std::runtime_error(("missing " + file).c_str());

	is.seekg(0, std::ios::end);
	size_t size = (size_t)is.tellg();
	is.seekg(0, std::ios::beg);

	sieved.resize(size);

	is.read((char*)&sieved[0], size);
}

SievedPrimes::Exhausted SievedPrimes::testPrime(unsigned val, bool& result) const
{
	unsigned ix = val / 8;
	if (ix >= sieved.size())
		return true;

	unsigned off = val % 8;
	result = !!(sieved[ix] & (1 << off));
	return false;
}

bool SievedPrimes::testPrime(unsigned val) const
{
	bool ret = false;
	testPrime(val, ret);
	return ret;
}

PrimeIterator::PrimeIterator()
	: sp(0), current(0)
{
}

PrimeIterator::PrimeIterator(SievedPrimes const& sp)
	: sp(&sp), current(2)
{
}

void PrimeIterator::increment()
{
	if (sp == 0) return;

	bool isPrime = false;
	while (!sp->testPrime(++current, isPrime))
		if (isPrime) return;

	sp = 0;
}

bool PrimeIterator::equal(PrimeIterator const& other) const
{
	if (sp != other.sp)
		return false;
	if (sp == 0)
		return true;
	return current == other.current;
}

unsigned const& PrimeIterator::dereference() const
{
	return current;
}