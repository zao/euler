#include "problem.h"
#include "primes.h"
#include "bits.h"
#include "spam.h"

#include <algorithm>
#include <deque>
#include <iostream>
#include <sstream>

int ipow(int base, int N)
{
	int ret = 1;
	while (N--)
		ret *= base;
	return ret;
}

template <>
void Problem<51>()
{
	int const N = 6;
	Primes ps = Primes(ipow(10, N));

	ps.primes.erase(ps.primes.begin(), ps.primes.lower_bound(ipow(10, N-1)));

	std::vector<std::vector<bool>> bitSets(ipow(10,N));
	for (int i = 0; i < ipow(2, N); ++i)
	{
		std::deque<bool> bits;
		bitsSet(bits, i, N);
		std::vector<bool>(bits.begin(), bits.end()).swap(bitSets[i]);
	}

	for (auto I = ps.primes.begin(); I != ps.primes.end(); ++I)
	{
		std::ostringstream oss;
		oss << *I;
		std::string s = oss.str();
		int N = s.size();

		static int x = 0;
		if (++x % 100 == 0)
			std::cerr << s << std::endl;

		for (int i = 0; i < ipow(2, N); ++i)
		{
			std::set<int> friendSet;
			std::string work = s;
			std::set<char> digits;
			std::vector<bool> const& bits = bitSets[i];

			for (int dig = '0'; dig <= '9'; ++dig)
			{
				for (int ix = 0; ix < N; ++ix)
				{
					if (bits[ix])
						work[ix] = dig;
				}
				char const* p = work.c_str();
				if (work[0] == '0')
					continue;

				int asdf = atoi(p);
				if (ps.primes.count(asdf))
				{
					friendSet.insert(asdf);
				}
			}
			if (friendSet.size() >= 8)
			{
				std::cout << s << " - " << friendSet << std::endl;
				return;
			}
		}
	}
}
