#pragma once
#include "problem.h"
#include "spam.h"

#include <algorithm>
#include <iterator>
#include <map>
#include <numeric>
#include <set>

namespace { enum { PROBLEM_LIMIT = 100 }; }

std::map<std::pair<int, int>, int> memo;
static int memoize(int items, int cap, int ways)
{
	return memo[std::make_pair(items, cap)] = ways;
}

int fencePost(int items, int cap)
{		
	if (cap > items)
		cap = items;

	auto I = memo.find(std::make_pair(items, cap));
	if (I != memo.end())
		return I->second;

	if (items <= 0)
		return memoize(items, cap, 1);
	if (items == 1 && cap == 1)
		return memoize(items, cap, 1);

	int ways = 0;
	for (int i = 1; i <= cap; ++i)
	{
		ways += fencePost(items - i, std::min(i, cap));
	}
	
	return memoize(items, cap, ways);
}

template <>
void Problem<76>()
{
	int limit = PROBLEM_LIMIT;
	int ways = 0;
	
	Stopwatch sw;
	for (int i = 1; i < limit; ++i)
	{
		ways += fencePost(limit - i , i);
	}

	std::cout << ways << " in " << sw.toc() << "s." << std::endl;
}