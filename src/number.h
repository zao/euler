#if 0
#include <boost/preprocessor.hpp>

#if !BOOST_PP_IS_ITERATING
#ifndef EULER_NUMBER_H
#define EULER_NUMBER_H

BOOST_PP_ITERATION_PARAMS_1(3, (1, 5, "number.h"))
BOOST_PP_ITERATE()

#endif
#else



#endif
#endif
#pragma once

#define NUM2(a0,a1) (a0 ## a1)
#define NUM3(a0,a1,a2) (a0 ## a1 ## a2)
#define NUM4(a0,a1,a2,a3) (a0 ## a1 ## a2 ## a3)