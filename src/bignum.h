#pragma once
#include <cmath>
#include <deque>
#include <iomanip>
#include <sstream>
#include <string>

#include <boost/cstdint.hpp>
#include <boost/algorithm/string.hpp>

template <typename Base>
struct BigTraits;

template <>
struct BigTraits<boost::uint32_t>
{
	typedef boost::uint32_t type;
	static type const mask = 1000000000u;
	static size_t const digits = 9u;
};

inline boost::uint32_t ipow(boost::uint32_t base, boost::uint32_t exponent)
{
	boost::uint32_t ret = 1;
	while (exponent--)
	{
		ret *= base;
	}
	return ret;
}

template <typename Base = boost::uint32_t>
struct BigNum
{
	std::deque<Base> contents;
	static Base const mask = BigTraits<Base>::mask;
	static size_t const digits = BigTraits<Base>::digits;

	explicit BigNum(Base val = Base());
	explicit BigNum(std::string const& source);

	BigNum operator + (BigNum const& other) const;
	BigNum& operator += (BigNum const& other);

	BigNum operator * (BigNum const& other) const;
	BigNum& operator *= (BigNum const& other);

	bool operator != (BigNum const& other) const;

	std::string str() const;
};

template <typename Base>
std::ostream& operator << (std::ostream& os, BigNum<Base> const& num)
{
	return os << num.str();
}

template <typename Base>
BigNum<Base>::BigNum(Base val)
{
	do
	{
		contents.push_back(val % mask);
		val /= mask;
	} while (val >= mask);
}

template <typename Base>
BigNum<Base>::BigNum(std::string const& source)
{
	std::string s = boost::trim_left_copy_if(source, boost::is_any_of("0"));
	Base current = Base();
	size_t used = 0;
	for (int i = s.size() - 1; i >= 0; --i)
	{
		if (used == digits)
		{
			contents.push_back(current);
			current = Base();
			used = 0;
		}
		current += ipow(10, used) * (s[i] - '0');
		++used;
	}
	if (used)
		contents.push_back(current);
}

template <typename Base>
BigNum<Base> BigNum<Base>::operator + (BigNum const& other) const
{
	BigNum ret = *this;
	return ret += other;
}

template <typename Base>
BigNum<Base>& BigNum<Base>::operator += (BigNum const& other)
{
	std::deque<Base> rhs = other.contents;
	bool smaller = rhs.size() > contents.size();
	if (smaller)
		contents.resize(rhs.size());
	Base carry = Base();
	for (size_t i = 0; i < rhs.size(); ++i)
	{
		contents[i] += rhs[i] + carry;
		carry = contents[i] / mask;
		contents[i] %= mask;
	}
	if (!smaller && carry)
		contents.push_back(carry);

	return *this;
}

template <typename Base>
BigNum<Base> BigNum<Base>::operator * (BigNum<Base> const& other) const
{
	BigNum<Base> ret = *this;
	return *this *= other;
}

template <typename Base>
BigNum<Base>& BigNum<Base>::operator *= (BigNum<Base> const& other)
{
	BigNum<Base> self = *this;
	BigNum<Base> one(1);
	for (BigNum<Base> i(1); i != other; i += one)
	{
		*this += self;
	}
	return *this;
}

template <typename Base>
bool BigNum<Base>::operator != (BigNum<Base> const& other) const
{
	if (contents.size() != other.contents.size()) return true;
	for (size_t i = 0; i < contents.size(); ++i)
		if (contents[i] != other.contents[i]) return true;
	return false;
}

template <typename Base>
std::string BigNum<Base>::str() const
{
	if (contents.empty())
		return "0";

	std::ostringstream oss;
	oss << contents.back();

	for (auto I = contents.rbegin() + 1; I != contents.rend(); ++I)
	{
//		oss << "<";
		oss << std::setw(digits) << std::setfill('0');
		oss << *I;
//	   	oss << ">";
	}
	return oss.str();
}

template <typename Base>
BigNum<Base> reverse(BigNum<Base> const& num)
{
	std::string s = num.str();
	std::reverse(s.begin(), s.end());
	return BigNum<Base>(s);
}
