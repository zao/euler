#include <iostream>
#include <string>
#include <utility>

int main()
{
	std::string line;
	size_t digSum = 0u;
	while (std::getline(std::cin, line))
	{
		size_t sum = 0u;
		for (auto I = line.begin(); I != line.end(); ++I)
		{
			sum += *I - '0';
		}
		digSum = std::max(sum, digSum);
		std::cout << sum << ' ' << line << std::endl;
	}
}
