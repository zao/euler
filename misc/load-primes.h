#include <cassert>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <set>
#include <sqlite3.h>

int main()
{
	sqlite3* db = NULL;
	sqlite3_open("primes.db", &db);
	sqlite3_exec(db, "PRAGMA journal_mode = OFF;", NULL, NULL, NULL);
	sqlite3_exec(db, "PRAGMA synchronous = OFF;", NULL, NULL, NULL);
	assert(db);
	sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS prime (value INTEGER PRIMARY KEY);", NULL, NULL, NULL);
	{
		sqlite3_stmt* stmt = NULL;
		sqlite3_prepare_v2(db, "INSERT OR IGNORE INTO prime VALUES (?);", -1, &stmt, NULL);
		assert(stmt);
		std::ifstream is("1000000000.primes");
		std::string s;

		sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
		long long x;
		int i = 0;
		while (is >> x)
		{
			sqlite3_reset(stmt);
//			sqlite3_bind_text(stmt, 1, s.c_str(), s.size(), SQLITE_TRANSIENT);
			sqlite3_bind_int64(stmt, 1, x);
			auto hr = sqlite3_step(stmt);
			if (++i % 10000 == 0)
			{
				sqlite3_exec(db, "COMMIT TRANSACTION;", NULL, NULL, NULL);
				std::cerr << std::setw(10) << x << std::endl;
				sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
			}
		}
		sqlite3_exec(db, "COMMIT TRANSACTION;", NULL, NULL, NULL);
		sqlite3_finalize(stmt);
	}
	sqlite3_close(db);
}
