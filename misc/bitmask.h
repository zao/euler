#include <iostream>
#include <fstream>
#include <string>
#include <boost/lexical_cast.hpp>

int main(int argc, char* argv[])
{
	if (argc != 2) return 1;

	std::ifstream is((std::string(argv[1]) + ".primes").c_str());
	std::ofstream os((std::string(argv[1]) + ".bits").c_str(), std::ios::binary);

	unsigned char octet = 0u;
	int current = -1;
	int limit = boost::lexical_cast<int>(argv[1]);
	for (int i = 0; ; ++i)
	{
		while (current < i && is >> current);
		if (current < i)
		{
			os.put(octet);
			break;
		}
		octet |= (current == i) << (i % CHAR_BIT);
		if (i % CHAR_BIT == CHAR_BIT - 1)
		{
			os.put(octet);
			octet = 0u;
		}
	}
}
