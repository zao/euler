VARIANT ?= release

all:
	bjam toolset=gcc $(VARIANT)

run: all
	bin/gcc-4.4.5/$(VARIANT)/euler 2>&1

.PHONY: all
